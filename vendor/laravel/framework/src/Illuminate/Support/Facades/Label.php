<?php

namespace Illuminate\Support\Facades;

/**
 * @see \Illuminate\Label\Factory
 */
class Label extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'label';
    }
}
