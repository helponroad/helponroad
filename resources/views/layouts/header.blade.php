<header class="navbar navbar-fixed">
  <div class="container">
    <div class="inner">

      <!-- Site Logo -->
      <a href="" class="site-logo">
        <img src="{{ asset('img/logo.svg') }}" alt="HelpOnRoad">
      </a><!-- .site-logo -->

      <!-- Mobile Dropdown -->
      <div class="mobile-dropdown">

        <!-- Mobile Tools -->
        <div class="mobile-tools">
          <form method="post" class="search-box">
            <input type="text" placeholder="Enter your search query">
            <button type="submit"><i class="pe-7s-search"></i></button>
          </form><!-- .search-box -->
          <div class="social-bar text-center">
            <a href="#" class="sb-facebook"><i class="fa fa-facebook"></i></a>
            <a href="#" class="sb-twitter"><i class="fa fa-twitter"></i></a>
            <a href="#" class="sb-google-plus"><i class="fa fa-google-plus"></i></a>
            <a href="#" class="sb-instagram"><i class="fa fa-instagram"></i></a>
          </div><!-- .social-bar -->
        </div><!-- .mobile-tools -->

        <!-- Main Navigation -->
        <nav class="main-navigation">
          <ul class="menu">
            <li class="menu-item-has-children current-menu-item">
              <a href="#">Home<span>&#8211;</span></a>
              <ul class="sub-menu">
                <li class="current-menu-item"><a href="index.html">Home Version 1</a></li>
                <li><a href="home-v2.html">Home Version 2</a></li>
                <li><a href="home-v3.html">Home Version 3</a></li>
                <li><a href="home-v4.html">Home Version 4</a></li>
              </ul>
            </li>
            <li class="menu-item-has-children">
              <a href="#">Blog<span>&#8211;</span></a>
              <ul class="sub-menu">
                <li><a href="blog-grid-boxed.html">Blog Grid Boxed</a></li>
                <li><a href="blog-grid-fullwidth.html">Blog Grid Full Width</a></li>
                <li><a href="blog-list-rs.html">Blog List Right Sidebar</a></li>
                <li><a href="blog-list-ls.html">Blog List Left Sidebar</a></li>
                <li><a href="blog-list-ns.html">Blog List No Sidebar</a></li>
                <li><a href="single-rs.html">Single Post Right Sidebar</a></li>
                <li><a href="single-ls.html">Single Post Left Sidebar</a></li>
                <li><a href="single-ns.html">Single Post No Sidebar</a></li>
              </ul>
            </li>
            <li><a href="gallery.html">Gallery</a></li>
            <li class="menu-item-has-children">
              <a href="#">Pages<span>&#8211;</span></a>
              <ul class="sub-menu">
                <li><a href="help-center.html">Help Center</a></li>
                <li><a href="contacts.html">Contacts</a></li>
                <li><a href="404.html">404</a></li>
              </ul>
            </li>
            <li>
              <a href="components.html">Components</a>
            </li>
          </ul><!-- .menu -->
        </nav><!-- .main-navigation -->
      </div><!-- .mobile-dropdown -->

      <!-- Toolbar -->
      <div class="toolbar">
        <div class="search-btn">
          <i class="pe-7s-search"></i>
          <form method="post" class="search-box">
            <input type="text" placeholder="Enter your search query">
            <button type="submit"><i class="pe-7s-search"></i></button>
          </form>
        </div>
        <span class="divider"></span>
        <div class="share-btn">
          <i class="pe-7s-share"></i>
          <ul class="dropdown">
            <li><a href="#" class="sb-facebook"><i class="fa fa-facebook"></i>Facebook</a></li>
            <li><a href="#" class="sb-twitter"><i class="fa fa-twitter"></i>Twitter</a></li>
            <li><a href="#" class="sb-google-plus"><i class="fa fa-google-plus"></i>Google+</a></li>
            <li><a href="#" class="sb-instagram"><i class="fa fa-instagram"></i>Instagram</a></li>
          </ul>
        </div>
        <a href="#markets" class="btn btn-sm btn-primary btn-icon-left scroll-to" data-offset-top="115">
          <i class="pe-7s-download"></i>Get App
        </a>
        <!-- Mobile Menu Toggle -->
        <div class="nav-toggle"><span></span></div>
      </div><!-- .toolbar -->
    </div><!-- .inner -->
  </div><!-- .container -->
</header><!-- .navbar.navbar-fixed -->
