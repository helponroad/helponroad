<html>
    <head>
      <meta charset="utf-8">
      <title>HelpOnRoad | Home Version 1</title>

      <!--SEO Meta Tags-->
      <meta name="description" content="HelpOnRoad - Multi-Purpose App Showcase Template" />
      <meta name="keywords" content="multipurpose, app showcase, mobile, blog, business, corporate, startup, landing, shortcodes, html5, css3, jquery, js, gallery, slider, touch, creative" />
      <meta name="author" content="Rokathemes" />

      <!--Mobile Specific Meta Tag-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

      <!--Favicon-->
      <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
      <link rel="icon" href="favicon.ico" type="image/x-icon">

      <!-- All Theme Styles including Bootstrap, Bourbon, Pixeden, etc. compiled from styles.scss-->
      <link href="{{ asset('css/styles.min.css') }}" rel="stylesheet" media="screen">
      <!-- Pixeden Icon Font -->
      <link href="{{ asset('css/vendor/pixeden.min.css') }}" rel="stylesheet" media="screen">

      <!-- Feather Icon Font -->
      <link href="{{ asset('css/vendor/feather-icons.min.css') }}" rel="stylesheet" media="screen">

      <!-- FontAwesome Icon Font -->
      <link href="{{ asset('css/vendor/fontawesome.min.css') }}" rel="stylesheet" media="screen">

      <!-- Bootstrap -->
      <link href="{{ asset('css/vendor/bootstrap.min.css') }}" rel="stylesheet" media="screen">

      <!-- Theme Styles -->
      <link href="{{ asset('css/styles.min.css') }}" rel="stylesheet" media="screen">

      <!--Color Scheme-->
      <!-- <link class="color-scheme" href="css/colors/color-ff3366.css" rel="stylesheet" media="screen"> -->

      <!--Modernizr / Detectizr-->
      <script type="text/javascript" src="{{ asset('js/vendor/modernizr.custom.js') }}"></script>

      <!-- Page Preloader (Pace) -->
      <script type="text/javascript" src="{{ asset('js/vendor/pace.min.js') }}"></script>
    </head>
        <body class="is-preloader parallax">
            <!-- Page Wrapper -->
            <div class="page-wrapper">
            @section('header')
                @include('layouts.header')
            @show
            </div>
            <div class="container">
                @yield('content')
            </div>
        </div>
      <!-- JavaScript (jQuery) libraries, plugins and custom scripts -->
      <script type="text/javascript" src="{{ asset('js/vendor/jquery-2.1.4.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/vendor/placeholder.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/vendor/smoothscroll.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/vendor/jquery.stellar.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/vendor/velocity.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/vendor/jquery.parallax.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/vendor/magnific-popup.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/vendor/owl.carousel.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/scripts.js') }}"></script>
    </body>
</html>