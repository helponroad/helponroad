@include('layouts.master')
<section class="intro-section" style="background-image: url({{ asset('img/home/intro/intro-bg01.jpg') }});" data-stellar-background-ratio="0.65">
  <span class="overlay" style="opacity: .85;"></span>
  <div class="container text-center">
    <div class="intro-logo space-bottom">
      <img src="{{ asset('img/home/intro/logo.png') }}" alt="HelpOnRoad">
    </div><!-- .intro-logo -->
    <p class="text-light opacity-50">Spread the word about HelpOnRoad</p>
    <ul class="sharing-links light-skin space-bottom">
      <li><a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i>6 358</a></li>
      <li><a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i>8 242</a></li>
      <li><a href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fa fa-instagram"></i>5 176</a></li>
    </ul><!-- .sharing-links -->
  </div><!-- .container -->
 </section>
<section class="fw-section bg-white border-top padding-top-3x padding-bottom-3x">
  <div class="container">
		<form action="" method="POST">
			<div class="form-group">
				 <label for="usr"><?php echo Label::get('label_username'); ?></label>
				<input type="text" name="username" value="<?php echo $user->username; ?>">
			</div>
			<div class="form-group">
				 <label for="usr"><?php echo Label::get('label_password'); ?></label>
				<input type="password" name="password">
			</div>
			<div class="form-group">
				 <label for="usr">First Name:</label>
				<input type="text" name="firstName">
			</div>
			<div class="form-group">
				 <label for="usr">Last Name:</label>
				<input type="text" name="lastName">
			</div>
			<div class="form-group">
				 <label for="usr">Email:</label>
				<input type="text" name="email">
			</div>
			<div class="form-group">
				 <label for="usr">City:</label>
				<input type="text" name="city">
			</div>
			<div class="form-group">
				 <label for="usr">Zip:</label>
				<input type="text" name="zip">
			</div>
			<div class="form-group">
				 <label for="usr">Address:</label>
				<input type="text" name="address">
			</div>
			<div class="form-group">
				 <label for="usr">Role:</label>
				<input type="text" name="role">
			</div>
			<div class="form-group">        
		      		<div class="col-sm-10">
		        			<button type="submit" class="btn btn-primary">Submit</button>
		        		</div>
		      	</div>
		    </div>
		</form>
	</div>
</section>
