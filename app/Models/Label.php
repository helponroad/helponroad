<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Foundation\Application;

class Label extends Model
{
    protected $table = 'label';

    public static function get($id) {
        $label = Label::where('label_id',$id)->first();
        if (count($label)) {
            if (app()->getLocale() == 'en') {
                return $label->en;
            } else if (app()->getLocale() == 'sr') {
                return $label->sr;
            } else if (app()->getLocale() == 'de') {
                return $label->de;
            } else if (app()->getLocale() == 'ru') {
                return $label->ru;
            }
        } else {
            echo '!!!-'.$id.'-!!!';
        }
    }
}
