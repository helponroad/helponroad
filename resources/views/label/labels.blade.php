@include('layouts.master')
      <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<section class="intro-section" style="background-image: url({{ asset('img/home/intro/intro-bg07.png') }});" data-stellar-background-ratio="0.65">
  <span class="overlay" style="opacity: .85;"></span>
  <div class="container text-center">
    <div class="intro-logo space-bottom">
      <img src="{{ asset('img/home/intro/logo.png') }}" alt="HelpOnRoad">
    </div><!-- .intro-logo -->
    <p class="text-light opacity-50">Spread the word about HelpOnRoad</p>
    <ul class="sharing-links light-skin space-bottom">
      <li><a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i>6 358</a></li>
      <li><a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i>8 242</a></li>
      <li><a href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fa fa-instagram"></i>5 176</a></li>
    </ul><!-- .sharing-links -->
  </div><!-- .container -->
</section>

<section class="fw-section bg-white padding-top-3x padding-bottom-3x" id="markets">
      <div class="container text-center">
            <div class="row">
                <br><br><br><br>
                <div class="col-xs-12">
                    <form class="form-inline" role="form">
                        <div class="form-group">
                            <label for="id">ID:</label>
                            <input type="text" class="form-control" id="id">
                        </div>
                        <div class="form-group">
                            <label for="keywords">Keywords:</label>
                            <input type="text" class="form-control" id="keywords">
                        </div>
                        <div class="form-group">
                            <label for="select_language">Select language:</label>
                            <select class="form-control" id="select_language">
                                <option value="en">en</option>
                                <option value="de">de</option>
                                <option value="sr">sr</option>
                                <option value="ru">ru</option>
                            </select>
                        </div>

                        <button type="submit" class="btn btn-default"><?php echo Label::get('button_search') ?></button>
                    </form>
                </div>  
                <br><br><br><br>
                <div class="text-center col-xs-12">
                    <table style="border: 1px solid black">
                        <tbody>
                            <tr>
                                <td>
                                    <p class="text-success"><?= "Id"; ?></p>
                                </td>
                                <td>
                                    <p class="text-success"><?= "Label id"; ?></p>
                                </td>
                                <td>
                                    <p class="text-success"><?= "EN"; ?></p>
                                </td>
                                <td>
                                    <p class="text-success"><?= "DE"; ?></p>
                                </td>
                                <td>
                                    <p class="text-success"><?= "SR"; ?></p>
                                </td>
                                <td>
                                    <p class="text-success"><?= "RU"; ?></p>
                                </td>
                                <td>
                                    <p class="text-success">Edit</p>
                                </td>
                                <td>
                                    <p class="text-success">Delete</p>
                                </td>
                            </tr>
                            <?php foreach ($labels as $key => $label) : ?>
                                <tr>
                                    <td>
                                        <p class="text-success"><?= $label->id; ?></p>
                                    </td>
                                    <td>
                                        <p class="text-success"><?= $label->label_id; ?></p>
                                    </td>
                                    <td>
                                        <p class="text-success"><?= $label->en; ?></p>
                                    </td>
                                    <td>
                                        <p class="text-success"><?= $label->de; ?></p>
                                    </td>
                                    <td>
                                        <p class="text-success"><?= $label->sr; ?></p>
                                    </td>
                                    <td>
                                        <p class="text-success"><?= $label->ru; ?></p>
                                    </td>
                                    <td>
                                        <p><a href="#"><span class="glyphicon  glyphicon-edit text-primary" data-toggle="tooltip" title="Edit"></span></a></p>
                                    </td>
                                    <td>
                                        <p><a href="#"><span class="glyphicon glyphicon-remove text-danger" data-toggle="tooltip" title="Delete!"></span></a></p>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>