<?php

namespace Illuminate\Foundation\Label;

class Label extends Facade
{
    protected $table = 'label';

    public static function get($id) {
        $label = Label::find($id);
        if (count($label)) {
            if (app()->getLocale() == 'en') {
                return $label->en;
            } else if (app()->getLocale() == 'sr') {
                return $label->sr;
            } else if (app()->getLocale() == 'de') {
                return $label->de;
            } else if (app()->getLocale() == 'ru') {
                return $label->ru;
            }
        } else {
            return '!!!-'.$id.'-!!!';
        }
    }
}
