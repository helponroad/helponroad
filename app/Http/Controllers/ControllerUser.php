<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\User;

class ControllerUser extends Controller
{
    public function info($id) {
        $user = User::find($id);
        return view('user.info')->with('user', $user);;

    }
}
